// src/app/pages/home.ts 

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { HomeService } from "../../providers/home-service/home-service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [
    HomeService
  ]
})
export class HomePage {

  public lista_consultaSaldo = new Array<any>();
  public saldoTotal: void

  constructor(
    public navCtrl: NavController, 
    private _http: Http,
    private homeService: HomeService) {

  
  }
    ionViewDidLoad(){
      this.homeService.getConsultaConta().subscribe(
        data =>{

          const response = (data as any);
          const objeto_retorno = JSON.parse(response._body);
          this.lista_consultaSaldo = objeto_retorno['data']['operacoes'];
          this.saldoTotal = objeto_retorno['data']['saldo'];
        }),error =>{
          console.log(error);
        }
    }
    
}